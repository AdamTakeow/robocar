These informations are both public and created for myself to remember what to do.

The Robocar World Championship (OOCWC) github site:

https://github.com/nbatfai/robocar-emulator

Deadlines:

Deadline for submission of Team Qualification Paper: 7 April./A jelentkezési TQP-k beküldése
Qualification limit: 100/8 with 1 police car (100 gengszterből hetet 1 rendőrrel)
Deadline for submission of the latest source code version: 10 April./A csapatok forrásainak beküldése
Competition rounds: 11-14 April./Maga a verseny
Competition report: 15 April./Az eredmények

Point information:

A TQP beküldéséért adok 20 pontot, de csakis akkor, ha már a "The highlights of our project:" pont is szépen ki van töltve

where to upload the submission

https://groups.google.com/forum/?hl=hu#!forum/rcemu

Any other important informaion can be found at the github site on the link above.

