/**
 * @brief Justine - this is a rapid prototype for development of Robocar City Emulator
 *
 * @file myshmclient.cpp
 * @author  Norbert Bátfai <nbatfai@gmail.com>
 * @version 0.0.10
 *
 * @section LICENSE
 *
 * Copyright (C) 2014 Norbert Bátfai, batfai.norbert@inf.unideb.hu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 * Robocar City Emulator and Robocar World Championship
 *
 * desc
 *
 */

#include <myshmclient.hpp>
//#include <trafficlexer.hpp>
#include <cmath>
#include <climits>

char data[524288];

std::vector<justine::sampleclient::MyShmClient::Gangster> justine::sampleclient::MyShmClient::gangsters ( boost::asio::ip::tcp::socket & socket, int id,
    osmium::unsigned_object_id_type cop )
{

  boost::system::error_code err;

  size_t length = std::sprintf ( data, "<gangsters " );
  length += std::sprintf ( data+length, "%d>", id );

  socket.send ( boost::asio::buffer ( data, length ) );

  length = socket.read_some ( boost::asio::buffer ( data ), err );

  if ( err == boost::asio::error::eof )
    {

      // TODO

    }
  else if ( err )
    {

      throw boost::system::system_error ( err );
    }

  /* reading all gangsters into a vector */
  int idd {0};
  unsigned f, t, s;
  int n {0};
  int nn {0};

  std::vector<Gangster> gangsters;
 // google protobuf átítás V
      while ( std::sscanf ( data+nn, "<OK %d %u %u %u>%n", &idd, &f, &t, &s, &n ) == 4 )
        {
          nn += n;
          gangsters.push_back ( Gangster {idd, f, t, s} );
        }

  // we does not need short
 /* std::sort ( gangsters.begin(), gangsters.end(), [this, cop] ( Gangster x, Gangster y )
  {
    return dst ( cop, x.to ) < dst ( cop, y.to );
} );*/
  // dont forget to comment this above!

/* just write
  std::cout.write ( data, length );
  std::cout << "Command GANGSTER sent." << std::endl;
*/

  return gangsters;
}

std::vector<justine::sampleclient::MyShmClient::Cop> justine::sampleclient::MyShmClient::initcops ( boost::asio::ip::tcp::socket & socket )
{

  boost::system::error_code err;

  size_t length = std::sprintf ( data, "<init guided %s 10 c>", m_teamname.c_str() ); //1 helyett 10 volt

  socket.send ( boost::asio::buffer ( data, length ) );

  length = socket.read_some ( boost::asio::buffer ( data ), err );


  if ( err == boost::asio::error::eof )
    {

      // TODO

    }
  else if ( err )
    {

      throw boost::system::system_error ( err );
    }

  /* reading all gangsters into a vector */
  int idd {0};
  int f, t;
  char c;
  int n {0};
  int nn {0};
  std::vector<Cop> cops;

  while ( std::sscanf ( data+nn, "<OK %d %d/%d %c>%n", &idd, &f, &t, &c, &n ) == 4 )
    {
      nn += n;
      cops.push_back ( idd );
    }
/* just write
  std::cout.write ( data, length );
  std::cout << "Command INIT sent." << std::endl;
*/
  return cops;
}


int justine::sampleclient::MyShmClient::init ( boost::asio::ip::tcp::socket & socket )
{

  boost::system::error_code err;

  size_t length = std::sprintf ( data, "<init guided %s 1 c>", m_teamname.c_str() );

  socket.send ( boost::asio::buffer ( data, length ) );

  length = socket.read_some ( boost::asio::buffer ( data ), err );

  if ( err == boost::asio::error::eof )
    {

      // TODO

    }
  else if ( err )
    {

      throw boost::system::system_error ( err );

    }

  int id {0};
  std::sscanf ( data, "<OK %d", &id );
/* just write
  std::cout.write ( data, length );
  std::cout << "Command INIT sent." << std::endl;
*/
  return id;

}

void justine::sampleclient::MyShmClient::pos ( boost::asio::ip::tcp::socket & socket, int id )
{

  boost::system::error_code err;

  size_t length = std::sprintf ( data, "<pos " );
  length += std::sprintf ( data+length, "%d %u %u>", id, 2969934868u, 651365957u );

  socket.send ( boost::asio::buffer ( data, length ) );

  length = socket.read_some ( boost::asio::buffer ( data ), err );

  if ( err == boost::asio::error::eof )
    {

      // TODO

    }
  else if ( err )
    {

      throw boost::system::system_error ( err );

    }
/* just write
  std::cout.write ( data, length );
  std::cout << "Command POS sent." << std::endl;
*/
}

void justine::sampleclient::MyShmClient::car ( boost::asio::ip::tcp::socket & socket, int id, unsigned *f, unsigned *t, unsigned* s )
{

  boost::system::error_code err;

  size_t length = std::sprintf ( data, "<car " );
  length += std::sprintf ( data+length, "%d>", id );

  socket.send ( boost::asio::buffer ( data, length ) );

  length = socket.read_some ( boost::asio::buffer ( data ), err );

  if ( err == boost::asio::error::eof )
    {

      // TODO

    }
  else if ( err )
    {

      throw boost::system::system_error ( err );
    }

  int idd {0};
  std::sscanf ( data, "<OK %d %u %u %u", &idd, f, t, s );
/* just write
  std::cout.write ( data, length );
  std::cout << "Command CAR sent." << std::endl;
*/
}

void justine::sampleclient::MyShmClient::route (
  boost::asio::ip::tcp::socket & socket,
  int id,
  std::vector<osmium::unsigned_object_id_type> & path
)
{

  boost::system::error_code err;

  size_t length = std::sprintf ( data,
                                 "<route %d %d", path.size(), id );

  for ( auto ui: path )
    length += std::sprintf ( data+length, " %u", ui );

  length += std::sprintf ( data+length, ">" );

  socket.send ( boost::asio::buffer ( data, length ) );

  length = socket.read_some ( boost::asio::buffer ( data ), err );

  if ( err == boost::asio::error::eof )
    {

      // TODO

    }
  else if ( err )
    {

      throw boost::system::system_error ( err );

    }
/* just write
  std::cout.write ( data, length );
  std::cout << "Command ROUTE sent." << std::endl;
*/
}

void justine::sampleclient::MyShmClient::start ( boost::asio::io_service& io_service, const char * port )
{

#ifdef DEBUG
  foo();
#endif

  boost::asio::ip::tcp::resolver resolver ( io_service );
  boost::asio::ip::tcp::resolver::query query ( boost::asio::ip::tcp::v4(), "localhost", port );
  boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve ( query );

  boost::asio::ip::tcp::socket socket ( io_service );
  boost::asio::connect ( socket, iterator );

  int id = init ( socket );

  pos ( socket, id );

  unsigned int g {0u};
  unsigned int f {0u};
  unsigned int t {0u};
  unsigned int s {0u};

  std::vector<Gangster> gngstrs;

 // startof MYEDITLEL
 /*std::vector<double> dist;
 std::vector<double> dist_old;
 std::vector<double> diffs;

 int mycounter = 0;
 int my_min_index = 0;
 int diffs_size = 0;
 int diffs_size_old = 0;
 int s_witch = 1;*/
 // endof MYEDITLEL

  for ( ;; )
    {
      std::this_thread::sleep_for ( std::chrono::milliseconds ( 200 ) );

      car ( socket, id, &f, &t, &s );

      gngstrs = gangsters( socket, id, t);
      // calculate the distances

      // startof MYEDITLEL
     /* for( auto it: gngstrs )
      {
          dist.push_back( dst( f, it.to ) );
      }

      if( dist_old.size() > 0 && dist.size() > 0)
      {
          for( auto it = 0 ; it < dist.size() ; it++ )
          {
              // calculate the differences according to the previous iteration
              diffs.push_back( dist_old.at( it ) - dist.at( it ) );
          }
      }*/
      // endof MYEDITLEL

     // diffs_size = diffs.size(); // MYEDITLEL

     /* just testing
       for( auto it: diffs )
       {
         std::cout << it << " ";
       }
       std::cout << std::endl;
     */

      // now we have to find the min in the diffs vector to be exact we need the min's index

      // startof MYEDITLEL
      // do this in every 125th iteration
      // 75 = 15 sec
      // 25 = 5 sec
      // 50 = 10 sec
      // 10 = 2 sec
      // 100 = 20 sec

      // every 10th second we calculate the gangster who has come closer to me than
      // he was in the previos iteration, and follow him through 10 sec
   //   if( ( mycounter % 50 == 0 ) /*|| ( diffs_size != diffs_size_old )*/ )
   /*   {
          if( diffs_size != diffs_size_old || s_witch == 1 )
          {
              double my_min = INT_MAX;
              my_min_index = 0;
              int temp = 0;
              for( auto it: diffs )
              {
                  // std::cout << "diff" << it << std::endl;
                  if( it < my_min )
                  {
                      my_min = it;
                      my_min_index = temp;
                  }
                  temp++;
              }
              std::cout << "I follow the one who come closer: " << my_min_index << std::endl;
              mycounter = 1;
              s_witch = 0;
          }else{ // after that, we calculate the nearest and follow him through 10 sec
              // calculate the nearest
              double my_min = INT_MAX;
              my_min_index = 0;
              int temp = 0;
              for( auto it: gngstrs )
              {
                  if( dst( f, it.to ) < my_min )
                  {
                      my_min = dst( f, it.to );
                      my_min_index = temp;
                  }
                  temp++;
              }
              std::cout << "I follow the nearest: " << my_min_index << std::endl;
              s_witch = 1;
          }
      }
*/
      // endof MYEDITLEL
      // std::cout << "my_min: " << my_min << std::endl;

      // now the my_min_index variable contains the gangster's index which seems to be closer to me
      // than he was at the previous iteration

      if ( gngstrs.size() > 0 )
      {
          g = gngstrs[0].to;
        //   g = gngstrs[ my_min_index ].to; // MYEDITLEL
      }
      else
          g = 0;
      if ( g > 0 )
        {
          std::vector<osmium::unsigned_object_id_type> path = hasDijkstraPath ( f, g );
          // std::cout << " GOT THE PATH " << std::endl;

          if ( path.size() > 1 )
            {
/* output ->
              std::copy ( path.begin(), path.end(),
                          std::ostream_iterator<osmium::unsigned_object_id_type> ( std::cout, " -*> " ) );
*/
              route ( socket, id, path );
              // std::cout << " IM ON MY WAY " << std::endl;
            }
        }

      // save this iteration's distances
  //    dist_old = dist; // MYEDITLEL

      // clear the fresh data
  //    dist.clear(); // MYEDITLEL
  //    diffs.clear(); // MYEDITLEL

   //   diffs_size_old = diffs_size; // MYEDITLEL

   //   mycounter++; // MYEDITLEL
    }
}

// do this shit
void justine::sampleclient::MyShmClient::start10 ( boost::asio::io_service& io_service, const char * port )
{

#ifdef DEBUG
  foo();
#endif

  boost::asio::ip::tcp::resolver resolver ( io_service );
  boost::asio::ip::tcp::resolver::query query ( boost::asio::ip::tcp::v4(), "localhost", port );
  boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve ( query );

  boost::asio::ip::tcp::socket socket ( io_service );
  boost::asio::connect ( socket, iterator );

  std::vector<Cop> cops = initcops ( socket );

  unsigned int g {0u};
  unsigned int f {0u};
  unsigned int t {0u};
  unsigned int s {0u};

  std::vector<Gangster> gngstrs;

  // startof MYEDITLEL
  std::vector< std::vector<double> > dist;
  std::vector< std::vector<double> > dist_old;
  std::vector<double> diffs;

  int mycounter = 0;
  int my_min_index = 0;
  int diffs_size = 0;
  int diffs_size_old = 0;
  int s_witch = 1;
  // endof MYEDITLEL

  for ( ;; )
    {
      std::this_thread::sleep_for ( std::chrono::milliseconds ( 200 ) );

      int zsaru = 0; // MYEDITLEL

      for ( auto cop:cops )
        {
          // std::cout << "zsaru " << zsaru++ << " szolgalatra jelentkezik!" << std::endl;
          car ( socket, cop, &f, &t, &s );

          gngstrs = gangsters ( socket, cop, t );

          // startof MYEDITLEL
          //std::cout << zsaru << ". kopo: " << std::endl;

          // calculate distances
          std::vector<double> temp;

          for( auto it: gngstrs )
          {
              temp.push_back( dst( f, it.to ) ); // ATIR
          }

          dist.push_back(temp);
          //std::cout << "mycounter: " << mycounter << std::endl;

          if( gngstrs.size() > 0 && dist_old.at(zsaru).size() > 0 )
          {
              int sz = dist.at(zsaru).size();

              for( auto ti = 0; ti < sz; ti++ )
              {
                 diffs.push_back( dist_old.at(zsaru).at(ti) - dist.at(zsaru).at(ti) );
              }
          }

          /*diffs_size = diffs.size();
          std::cout << "diffs " << std::endl;
          for( auto it: diffs )
            {
              std::cout << it << " ";
            }
          std::cout << std::endl;
          */

          // now we have to find the min in the diffs vector to be exact we need the min's index

          // startof MYEDITLEL

          // std::cout << "mycounter: " << mycounter << std::endl;
          // std::cout << s_witch << std::endl;

          if( ( mycounter % 25 == 0 ) /*|| ( diffs_size != diffs_size_old ) */)
          {
              if( diffs_size != diffs_size_old || s_witch == 1 )
              {
                  double my_min = INT_MAX;
                  my_min_index = 0;
                  int temp = 0;
                  for ( auto it: diffs )
                  {
                      // std::cout << it << " ";
                      if( it < my_min )
                      {
                          my_min = it;
                          my_min_index = temp;
                      }
                      temp++;
                  }
                  // std::cout << std::endl;
                  std::cout << "I ( " << zsaru << " ) follow the one who come closer: " << my_min_index << std::endl;
                  // mycounter = 1; // this is the problem
                  // s_witch = 0; // also this
              }else{
                  // calculate the nearest
                  double my_min = INT_MAX;
                  my_min_index = 0;
                  int temp = 0;
                  for( auto it: gngstrs )
                  {
                      // std::cout << dst( f, it.to ) << " "; // ATIR
                      if( dst( f, it.to ) < my_min ) // ATIR
                      {
                          my_min = dst( f, it.to ); // ATIR
                          my_min_index = temp;
                      }
                      temp++;
                  }
                  // std::cout << std::endl;
                  std::cout << "I ( " << zsaru << " ) follow the nearest: " << my_min_index << std::endl;
                  // s_witch = 1; // problem
              }
          }
          // endof MYEDITLEL

          // now the my_min_index variable contains the gangster's index which seems to be closer to me
          // than he was at the previous iteration

          if ( gngstrs.size() > 0 )
            {
               // g = gngstrs[0].to;
              g = gngstrs[ my_min_index ].to; // MYEDITLEL
            }
            else
              g = 0;
          if ( g > 0 )
            {
              std::vector<osmium::unsigned_object_id_type> path = hasDijkstraPath ( f, g );

              if ( path.size() > 1 )
                {
/* output ->
                  std::copy ( path.begin(), path.end(),
                              std::ostream_iterator<osmium::unsigned_object_id_type> ( std::cout, " -> " ) );
*/
                  route ( socket, cop, path );
                  // std::cout << zsaru++ << " is routing to " << my_min_index << std::endl;
                }
            }

          diffs.clear();

          diffs_size_old = diffs_size;

          zsaru++;
        } // cops iteration end

      dist_old = dist;

      dist.clear();

      mycounter++;
      if( mycounter > 25 )
      {
          mycounter = 1;
          if( s_witch == 0 )
          {
              s_witch = 1;
          }else{
              s_witch = 0;
          }
      }
   } // for end
}
